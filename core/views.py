from .models import Product
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import ProductSerializer

# Create your views here.
class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint catalog.
    """
    queryset = Product.objects.all().order_by('brand')
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]