from django.test import TestCase
from django.contrib.auth.models import User
from .models import Product, Combination, Order, ItemOrder
from decimal import Decimal

# Create your tests here.
class ProductTestCase(TestCase):
    def setUp(self):

        self.user1 = User.objects.create_user('user1', None, 'test1234')

        self.product1 = Product.objects.create(brand='GEOX', name='Symbol')
        self.product2 = Product.objects.create(brand='GEOX', name='Iacopo')
        self.product3 = Product.objects.create(brand='Pikolinos', name='Cambil')

        self.combination1 = Combination.objects.create(product=self.product1, size=40, color='Azul Marino', price=99.9)
        self.combination2 = Combination.objects.create(product=self.product1, size=40, color='Negro', price=99.9)
        self.combination3 = Combination.objects.create(product=self.product2, size=39, color='Azul Marino', price=129.90)
        self.combination4 = Combination.objects.create(product=self.product3, size=44, color='Cuero', price=109.95)
        self.combination5 = Combination.objects.create(product=self.product3, size=44, color='Olmo', price=109.95)

        self.order1 = Order.objects.create(client=self.user1)
        self.item1 = ItemOrder.objects.create(order=self.order1, product=self.combination1, units=2)
        self.item1 = ItemOrder.objects.create(order=self.order1, product=self.combination2, units=3)
        self.item1 = ItemOrder.objects.create(order=self.order1, product=self.combination4, units=1)

    def test_update_product_on_new_combination(self):
        combination6 = Combination.objects.create(product=self.product1, size=41, color='Azul Marino', price=99.9)
        self.assertEqual(combination6.created, self.product1.updated)
        print("{} - {}".format(combination6.created, self.product1.updated))

    def test_update_product_on_update_combination(self):
        self.combination1.size = 41
        self.combination1.save()
        self.assertEqual(self.combination1.updated, self.product1.updated)
        print("{} - {}".format(self.combination1.updated, self.product1.updated)) 

    def test_correct_price_order(self):
        self.assertEqual(self.order1.total, Decimal('609.45'))


