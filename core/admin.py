from django.contrib import admin
from .models import Product, Combination, Order, ItemOrder

# Register your models here.
class CombinationInline(admin.TabularInline):
    list_display = ('color', 'size', 'price')
    model = Combination
    extra = 10

class NewItemOrderInline(admin.TabularInline):
    model = ItemOrder
    extra = 20

class ExistItemOrderInline(admin.TabularInline):
    model = ItemOrder
    extra = 0
    
    def has_add_permission(self, request):
        return False

class ProductAdmin(admin.ModelAdmin):
    list_display = ('brand', 'name')
    readonly_fields = ('updated',)
    inlines = [CombinationInline]

class OrderAdmin(admin.ModelAdmin):
    list_display = ('client', 'created', 'total')
    readonly_fields = ('client', 'created')

    # filtra los order que se muestran si admin todos si no solo los del usuario
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(client=request.user)

    # si es superusuario permite elejir el usuario al crear pedido si no no
    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return ['created']
        else:
            return ['client', 'created']

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.inlines = [ExistItemOrderInline]
        if request.user.is_superuser: self.inlines = [NewItemOrderInline]
        return super().change_view(request, object_id)

    def add_view(self, request, form_url='', extra_context=None):
        self.inlines = [NewItemOrderInline]
        return super().add_view(request)

    # asigna el usuario actual al guardar pedido
    def save_model(self, request, obj, form, change):
        # falta esta linea de codigo
        #if not request.user.is_superuser: 
        obj.client = request.user
        super().save_model(request, obj, form, change)

    #envia el correo al crear correo
    def response_add(self, request, obj=None):
        obj.send_mail_with_csv()
        return super().response_add(request, obj)

admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
