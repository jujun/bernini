import csv
from django.db import models
from django.contrib.auth.models import User
from io import StringIO
from django.core.mail import EmailMessage
from django.conf import settings

# Create your models here.
class Product(models.Model):
    brand = models.CharField(verbose_name="Marca", max_length=50)
    name = models.CharField(verbose_name="Modelo", max_length=50)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")
    
    class Meta:
        verbose_name = "producto"
        verbose_name_plural = "productos"
        ordering = ['brand', 'name']

    def __str__(self):
        return f"{self.brand} {self.name}"

class Combination(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='combinations')
    color = models.CharField(verbose_name="Color", max_length=50)
    size = models.SmallIntegerField(verbose_name="Talla")
    price = models.DecimalField(verbose_name="Precio", max_digits=6, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")
    
    class Meta:
        verbose_name = "combinacion"
        verbose_name_plural = "combinaciones"
        ordering = ['product__brand', 'product__name', 'color', 'size']

    def __str__(self):
        return f"{self.product} {self.color} {self.size}"
        
    # es para frozar la actualizacion del campo updated de product
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.product.save()

class Order(models.Model):
    client = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Cliente')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    @property
    def total(self):
        return sum([item.product.price * item.units for item in self.items.all()])

    def send_mail_with_csv(self):
        csvfile = StringIO()
        csvwriter = csv.writer(csvfile)
        labels = ['Marca', 'Modelos', 'Color', 'Talla', 'Unidades']
        csvwriter.writerow(labels)
        for item in self.items.all():
            csvwriter.writerow([item.product.product.brand, item.product.product.name, 
                item.product.color, item.product.size, item.units])
        message = EmailMessage("Nuevo pedido", f"Adjunto nuevo pedido de {self.client.username}", settings.EMAIL_FROM, [settings.EMAIL_TO])
        message.attach('pedido.csv', csvfile.getvalue(), 'text/csv')
        message.send(fail_silently=True)
    
    def __str__(self):
        return f"{self.client} {self.created}"

    class Meta:
        verbose_name = "pedido"
        verbose_name_plural = "pedidos"
        ordering = ['created']

class ItemOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Combination, on_delete=models.PROTECT)
    units = models.PositiveSmallIntegerField(verbose_name="unidades", default=0)

    def __str__(self):
        return f"{self.order} {self.product}"
