# Zapatos Bernini #

Proyecto de ejemplo de un sistema de catalogo y pedidos que incluye API

### Instalacion ###

* pip install -r requirements.txt
* Configurar el email en las variables EMAIL_HOST, EMAIL_PORT, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD de settings.py

### Administracion ###

* Usuario **administrador** Contraseña **django1234** Tipo **superuser** 
	* puede ver, crear, editar y eliminar tanto *productos* como *pedidos*
* Usuario **cliente1** Contraseña **django1234** Tipo **cliente** 
	* puede ver y crear sus propios *pedidos*, y solo ver *productos*

### API ###

* Puede consultarse la documentacion en /api/docs

