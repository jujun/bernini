from .models import Product, Combination
from rest_framework import serializers


class CombinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Combination
        fields = ['color', 'size', 'price']

class ProductSerializer(serializers.ModelSerializer):
    combinations = CombinationSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['brand', 'name', 'combinations']
